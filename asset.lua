function load_assets()
  local map = love.graphics.newImage("res/map.png")
  map:setFilter("nearest")
  assets = {
    mag = {
      bod = love.graphics.newImage("res/mag_bod.png"),
      bod_h = 64,
      arm_b = love.graphics.newImage("res/mag_arm_b.png"),
      arm_f = love.graphics.newImage("res/mag_arm_f.png"),
      leg = love.graphics.newImage("res/mag_leg.png"),
    },
    ulf = {
      bod = love.graphics.newImage("res/ulf_bod.png"),
      bod_h = 58,
      arm_b = love.graphics.newImage("res/ulf_arm_b.png"),
      arm_f = love.graphics.newImage("res/ulf_arm_f.png"),
      leg = love.graphics.newImage("res/ulf_leg.png"),
    },
    map = map,
    map_mask = love.image.newImageData("res/map_mask.png"),
    creep = {
      love.graphics.newImage("res/creep_1.png"),
      love.graphics.newImage("res/creep_2.png"),
    },
  }
end

function draw_ledare(ledare, player)
  local sx = 1
  if player.invert_img then
    sx = -1
  end
  if player.is_moving then
    draw_moving_player(ledare, player, sx)
  else
    draw_standing_player(ledare, player, sx)
  end
  draw_hp_bar(player)
end

arm = { 0, -1, -2, 0, 1 }
leg1 = { 0, -2, 0, 2 }
leg2 = { 0, 2, 0, -2 }

-- love.graphics.draw(ledare.bod, player.x, player.y, 0, sx, 1, offset = halva bredd)

function draw_hp_bar(player)
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.rectangle("fill", player.x - 18, player.y - 5, 30, 3)
  love.graphics.setColor(1.0, 0, 0)
  love.graphics.rectangle("fill", player.x - 18, player.y - 5, 30 * player.hp / player.max_hp, 3)
end

function draw_moving_player(ledare, player, sx)
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.draw(
    ledare.leg,
    player.x + (6 * sx),
    player.y + ledare.bod_h - 2 + leg1[player.leg_animation + 1],
    0,
    sx,
    1,
    2
  )
  love.graphics.draw(
    ledare.leg,
    player.x - (4 * sx),
    player.y + ledare.bod_h - 2 + leg2[player.leg_animation + 1],
    0,
    sx,
    1,
    2
  )
  love.graphics.draw(
    ledare.arm_b,
    player.x - ((16 + arm[player.arm_animation + 1]) * sx),
    player.y + 33,
    0,
    sx,
    1,
    3
  )
  love.graphics.draw(ledare.bod, player.x, player.y, 0, sx, 1, 13)
  love.graphics.draw(
    ledare.arm_f,
    player.x + ((4 + arm[player.arm_animation + 1]) * sx),
    player.y + 33,
    0,
    sx,
    1,
    3
  )
end

function draw_standing_player(ledare, player, sx)
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.draw(ledare.leg, player.x + (6 * sx), player.y + ledare.bod_h - 2, 0, sx, 1, 2)
  love.graphics.draw(ledare.leg, player.x - (4 * sx), player.y + ledare.bod_h - 2, 0, sx, 1, 2)
  love.graphics.draw(
    ledare.arm_b,
    player.x - ((16 + arm[player.arm_animation + 1]) * sx),
    player.y + 33,
    0,
    sx,
    1,
    3
  )
  love.graphics.draw(ledare.bod, player.x, player.y, 0, sx, 1, 13)
  love.graphics.draw(
    ledare.arm_f,
    player.x + ((4 + arm[player.arm_animation + 1]) * sx),
    player.y + 33,
    0,
    sx,
    1,
    3
  )
end

function draw_aim(player)
  love.graphics.setColor(1.0, 0.0, 0.0)
  love.graphics.circle("fill", player.aim_x, player.aim_y, 8.0)
end

function draw_creeps()
  for i, creep in pairs(creeps) do
    draw_creep(creep)
  end
end

function draw_creep(creep)
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.draw(assets.creep[creep.team], creep.x, creep.y, 0, sx, 1, 2)

  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.rectangle("fill", creep.x - 3, creep.y - 4, 20, 3)
  love.graphics.setColor(1.0, 0, 0)
  love.graphics.rectangle("fill", creep.x - 3, creep.y - 4, 20 * creep.hp / creep.max_hp, 3)
end

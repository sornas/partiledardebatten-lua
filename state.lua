local map = require("map")

Player = {
  speed = 100,
  hit_vel_x = 0,
  hit_vel_y = 0,
  hit_cooldown = 0,
  invert_img = true,
  is_moving = false,
  leg_animation = 1,
  arm_animation = 1,
  animation_delta = 0,
  asset = nil,
  hp = 100,
  max_hp = 100,
  aim_active = false,
  aim_x = 0,
  aim_y = 0,
  aim_shooting = false,
}

creeps = {}

cycle_time = 0
curr_cycle = 1
curr_spawn_count = 0
time_since_spawn = 0
Cycles = {
  {
    duration = 10,
    spawn_count = 5,
    spawn_frequency = 1,
  },
  {
    duration = 4,
    spawn_count = 0,
    spawn_frequency = 0,
  },
}

function init_players()
  players = {
    Player:create({
      id = 1,
      x = 100,
      y = 100,
      asset = assets.mag,
      collision = collision.makeRect(0, 0, 36, 64),
      aim_collision = collision.makeCircle(0, 0, 8),
    }),
    Player:create({
      id = 2,
      x = 200,
      y = 200,
      asset = assets.ulf,
      collision = collision.makeRect(0, 0, 36, 64),
      aim_collision = collision.makeCircle(0, 0, 8),
    }),
  }
end

function Player:create(o)
  setmetatable(o, self)
  self.__index = self
  return o
end

function spawn_creep(x, y, team)
  local creep = {}
  creep.x = x
  creep.y = y
  creep.hit_vel_x = 0
  creep.hit_vel_y = 0
  creep.max_hp = 20
  creep.hp = 20
  creep.collision = collision.makeRect(x, y, 19, 32)
  creep.team = team
  if team == 1 then
    creep.target = 2
  else
    creep.target = #nodes - 1
  end
  table.insert(creeps, creep)
end

function spawn_creep_one()
  spawn_creep(nexus[1][1], nexus[1][2], 1)
end

function spawn_creep_two()
  spawn_creep(nexus[2][1], nexus[2][2], 2)
end

function update_main_menu(delta, controllers) end

function draw_main_menu() end

function update_character_select(delta, controllers) end

function draw_character_select() end

function update_playing(delta, controllers)
  update_spawn_timer(delta)
  update_playing_player(delta, controllers[1], players[1])
  update_playing_player(delta, controllers[2], players[2])
  for c, creep in pairs(creeps) do
    update_creep(delta, creep)
  end
end

function update_creep(delta, creep)
  local target = nodes[creep.target]
  local node_dist = math.sqrt((creep.x - target[1]) ^ 2 + (creep.y - target[2]) ^ 2)
  -- if at target
  if node_dist < 5 then
    -- if no target: hit da nexus
    -- else: next target
    if creep.team == 1 then
      if creep.target ~= #nodes then
        creep.target = creep.target + 1
      else
        -- nexus
      end
    else
      if creep.target ~= 1 then
        creep.target = creep.target - 1
      else
        -- nexus
      end
    end
  end

  -- Update target and node dist if we changed target
  target = nodes[creep.target]
  node_dist = math.sqrt((creep.x - target[1]) ^ 2 + (creep.y - target[2]) ^ 2)

  -- if player close: aggro
  local target_player = players[3 - creep.team]
  local player_dist = math.sqrt((creep.x - target_player.x) ^ 2 + (creep.y - target_player.y) ^ 2)
  creep.aggro = player_dist < 100

  local walk_x
  local walk_y
  if creep.aggro then
    -- move towards player
    walk_x = (target_player.x - creep.x) / player_dist
    walk_y = (target_player.y - creep.y) / player_dist
    if player_dist < 10 then
      -- hit player
    end
  else
    -- move towards target
    walk_x = (nodes[creep.target][1] - creep.x) / node_dist
    walk_y = (nodes[creep.target][2] - creep.y) / node_dist
  end

  -- do the walk
  creep.x = creep.x + walk_x * 50 * delta
  creep.y = creep.y + walk_y * 50 * delta
  collision.moveTo(creep.collision, creep.x, creep.y)
end

function spawn_n_creeps(n)
  spawn_creep_one()
  spawn_creep_two()
end

function update_spawn_timer(delta)
  cycle_time = cycle_time + delta
  local cycle = Cycles[curr_cycle]
  if curr_spawn_count < cycle.spawn_count then
    to_spawn = math.floor((cycle_time - time_since_spawn) / cycle.spawn_frequency)
    if to_spawn > 0 then
      time_since_spawn = cycle_time
      curr_spawn_count = curr_spawn_count + to_spawn
      spawn_n_creeps(to_spawn)
    end
  end

  if cycle.duration <= cycle_time then
    cycle_time = cycle_time - cycle.duration
    time_since_spawn = 0
    curr_spawn_count = 0
    curr_cycle = curr_cycle % #Cycles + 1
    --print("changing to cycle ", curr_cycle)
  end
end

function update_playing_player(delta, controller, player)
  update_animation_delta(delta, player)
  if player.hit_cooldown > 0 then
    player.hit_cooldown = player.hit_cooldown - delta
    if player.hit_cooldown < 0 then
      player.hit_cooldown = 0
    end
  end
  if controller == nil or player == nil then
    return
  end

  if is_offroad(player.x + 13, player.y + player.asset.bod_h) then
    player.speed = 50
  else
    player.speed = 100
  end

  local old_x = player.x
  local old_y = player.y

  player.x = player.x + controller.axis.left.x * delta * player.speed
  player.y = player.y + controller.axis.left.y * delta * player.speed

  if math.abs(player.hit_vel_x) > 0 then
    player.x = player.x + player.hit_vel_x * delta
    player.hit_vel_x = player.hit_vel_x / 1.1
    if math.abs(player.hit_vel_x) <= 1 then
      player.hit_vel_x = 0
    end
  end
  if math.abs(player.hit_vel_y) > 0 then
    player.y = player.y + player.hit_vel_y * delta
    player.hit_vel_y = player.hit_vel_y / 1.1
    if math.abs(player.hit_vel_y) <= 1 then
      player.hit_vel_y = 0
    end
  end

  collision.moveTo(player.collision, player.x, player.y)

  if old_x == player.x and old_y == player.y then
    player.is_moving = false
  else
    player.is_moving = true
  end

  if controller.axis.right.x ^ 2 + controller.axis.right.y ^ 2 > 0.25 then
    player.aim_active = true
    local aim_x = controller.axis.right.x
    local aim_y = controller.axis.right.y
    local aim_len = math.sqrt(aim_x ^ 2 + aim_y ^ 2)
    player.aim_x = player.x + (aim_x / aim_len * 50)
    player.aim_y = player.y + 32 + (aim_y / aim_len * 50)
    collision.moveTo(player.aim_collision, player.aim_x, player.aim_y)
  else
    player.aim_active = false
  end

  if player.aim_active then
    if player.aim_x > 0 then
      player.invert_img = true
    elseif player.aim_x < 0 then
      player.invert_img = false
    else
    end
  else
    if controller.axis.left.x > 0 then
      player.invert_img = true
    elseif controller.axis.left.x < 0 then
      player.invert_img = false
    else
    end
  end

  player.aim_shooting = controller.trig.right > 0.75

  if player.aim_shooting and player.hit_cooldown == 0 then
    for c, creep in ipairs(creeps) do
      if
        player.id ~= creep.team and collision.collisionTest(player.aim_collision, creep.collision)
      then
        player.hit_cooldown = 1
        local dx = player.x - creep.x
        local dy = player.y + player.asset.bod_h / 2 - creep.y
        local dlen = dx ^ 2 + dy ^ 2
        player.hit_vel_x = dx / dlen * 10000
        player.hit_vel_y = dy / dlen * 10000
        creep.hit_vel_x = -dx / dlen * 8000
        creep.hit_vel_y = -dy / dlen * 8000
        creep.hp = creep.hp - 5
        if creep.hp <= 0 then
          table.remove(creeps, c)
        end
      end
    end

    local other_player = players[3 - player.id]
    if collision.collisionTest(player.aim_collision, other_player.collision) then
      player.hit_cooldown = 1
      local dx = player.x - other_player.x
      local dy = player.y - other_player.y
      local dlen = dx ^ 2 + dy ^ 2
      player.hit_vel_x = dx / dlen * 30000
      player.hit_vel_y = dy / dlen * 30000
      other_player.hit_vel_x = -dx / dlen * 10000
      other_player.hit_vel_y = -dy / dlen * 10000
    end
  end
end

function draw_playing()
  love.graphics.setColor(1.0, 1.0, 1.0)
  love.graphics.draw(assets.map, 0, 0, 0, map_scale, map_scale)
  draw_creeps()
  draw_ledare(players[1].asset, players[1])
  draw_ledare(players[2].asset, players[2])
  if players[1].aim_active then
    draw_aim(players[1])
  end
  if players[2].aim_active then
    draw_aim(players[2])
  end

  if DEBUG then
    map.draw_path()
  end
end

function update_animation_delta(delta, player)
  player.animation_delta = player.animation_delta + delta
  if player.animation_delta > 0.03 then
    player.animation_delta = player.animation_delta - 0.03
    player.leg_animation = (player.leg_animation + 1) % 4
    player.arm_animation = (player.arm_animation + 1) % 5
    -- spawn_creep({500, 500}, {400, 400}, 50, 1)
  end
end

function update_game_end(delta, controllers) end

function draw_game_end() end

function printJoyStick()
  local joysticks = love.joystick.getJoysticks()
  for j, joystick in ipairs(joysticks) do
    print("----------------------------")
    local name = joystick:getName()
    print(j, name)
    a1, a2, a3, a4, a5, a6 = joystick:getAxes()
    print(a1, a2, a3, a4, a5, a6)
  end
end

function unpackAxes(joystick)
  local name = joystick:getName()
  if name == "Xbox One S Controller" or string.find(name, "360") then
    axisLeftX, axisLeftY, axisLeftTrig, axisRightX, axisRightY, axisRightTrig = joystick:getAxes()
  elseif name == "BDA NSW Wired controller" then
    axisLeftX, axisLeftY, axisRightX, axisRightY, axisLeftTrig, axisRightTrig = joystick:getAxes()
  else
    print("unknown controller ", name)
  end
  return {
    axis = {
      left = {
        x = axisLeftX,
        y = axisLeftY,
      },
      right = {
        x = axisRightX,
        y = axisRightY,
      },
    },
    trig = {
      left = axisLeftTrig,
      right = axisRightTrig,
    },
  }
end

function showAxes(axes)
  print("  ", axes.axis.left.x)
  print("  ", axes.axis.left.y)
  print("  ", axes.axis.right.x)
  print("  ", axes.axis.right.y)
  print("  ", axes.trig.left)
  print("  ", axes.trig.right)
end

function getInput(numPlayers)
  local joystick = love.joystick.getJoysticks()[numPlayers]
  joystick = unpackAxes(joystick)
  if math.abs(joystick.axis.left.x) < 0.15 then
    joystick.axis.left.x = 0.0
  end
  if math.abs(joystick.axis.left.y) < 0.15 then
    joystick.axis.left.y = 0.0
  end
  -- if math.abs(axisRightX) < 0.15 then
  --   axisRightX = 0.0
  -- end
  -- if math.abs(axisRightY) < 0.15 then
  --   axisRightY = 0.0
  -- end
  return joystick
end

function getNumControllers()
  return #love.joystick.getJoysticks()
end

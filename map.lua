local M = {}

nexus = {
  { 200, 300 },
  { 1150, 500 },
}

nodes = {
  { 200, 300 },
  { 200, 400 },
  { 200, 450 },
  { 200, 500 },
  { 200, 550 },
  { 200, 600 },
  { 200, 650 },
  { 250, 700 },
  { 300, 700 },
  { 350, 700 },
  { 400, 700 },
  { 450, 700 },
  { 500, 700 },
  { 550, 700 },
  { 600, 650 },
  { 650, 650 },
  { 700, 650 },
  { 750, 650 },
  { 850, 650 },
  { 800, 600 },
  { 750, 550 },
  { 700, 500 },
  { 650, 400 },
  { 600, 300 },
  { 550, 250 },
  { 500, 200 },
  { 450, 150 },
  { 400, 100 },
  { 450, 100 },
  { 500, 100 },
  { 550, 100 },
  { 600, 100 },
  { 650, 100 },
  { 700, 100 },
  { 750, 100 },
  { 800, 100 },
  { 850, 100 },
  { 900, 100 },
  { 950, 100 },
  { 1000, 100 },
  { 1050, 100 },
  { 1050, 100 },
  { 1050, 150 },
  { 1050, 200 },
  { 1050, 250 },
  { 1100, 300 },
  { 1100, 350 },
  { 1100, 400 },
  { 1150, 500 },
}

map_scale = 1.832579185520362

function is_offroad(x, y)
  -- (x, y) is screen coordinates, need to scale down by two
  x = x / map_scale
  y = y / map_scale
  -- check color in mask
  if x < 0 or y < 0 or x > 1337 or x > 810 then
    return true
  end
  local r, g, b = assets.map_mask:getPixel(x, y)
  return r == 1
end

function M.draw_path()
  love.graphics.setColor(255, 0, 0)
  for _, node in ipairs(nodes) do
    love.graphics.circle("fill", node[1], node[2], 3)
  end
end

return M
